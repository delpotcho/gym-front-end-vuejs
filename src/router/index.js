import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard.vue'
import Projects from '../views/Projects.vue'
import Team from '../views/Team.vue'
import Coach from '../views/Coach.vue'
import Adherent from '../views/Adherent.vue'
import Calendar from '../views/Calendar.vue'
import Seance from '../views/Seance.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard
  },
  {
    path: '/projects',
    name: 'projects',
    component: Projects
  },
  {
    path: '/team',
    name: 'team',
    component: Team
  },
  {
    path: '/calendar',
    name: 'Calendar',
    component: Calendar
  },
  {
    path: '/Seance',
    name: 'seance',
    component: Seance
  },
  {
    path: '/Adherent',
    name: 'Adherent',
    component: Adherent
  },
  {
    path: '/Coach',
    name: 'Coach',
    component: Coach
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
