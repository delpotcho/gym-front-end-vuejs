import Vue from 'vue'
import App from './App.vue'
import router from './router'
import "bootstrap";
import"bootstrap/dist/css/bootstrap.min.css"
import vuetify from './plugins/vuetify';
import PrimeVue from 'primevue/config';
import Dialog from 'primevue/dialog';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup';
import 'primevue/resources/themes/saga-blue/theme.css';    //theme
import  'primevue/resources/primevue.min.css';                 //core css
import 'primeicons/primeicons.css';                           //icons
import Panel from 'primevue/panel';
import Menubar from 'primevue/menubar';
import InputText from 'primevue/inputtext';
import Password from 'primevue/password';
import Button from 'primevue/button';
import Message from 'primevue/message';
import ToastService from 'primevue/toastservice';
import Toast from 'primevue/toast';
Vue.use(ToastService);




Vue.use(PrimeVue);



Vue.component('Dialog', Dialog);
Vue.component('DataTable', DataTable);

Vue.component('Column', Column);
Vue.component('ColumnGroup', ColumnGroup);
Vue.component('Panel', Panel);
Vue.component('Menubar', Menubar);
Vue.component('InputText', InputText);
Vue.component('Password', Password);
Vue.component('Button', Button);
Vue.component('Message', Message);
Vue.component('Toast', Toast);



Vue.config.productionTip = false



new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
